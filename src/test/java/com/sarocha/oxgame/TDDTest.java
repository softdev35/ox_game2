/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.sarocha.oxgame;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Sarocha
 */
public class TDDTest {

    public TDDTest() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    @Test
    public void checkVertical1() {
        char table[][] = {{'O', '-', '-'},
                         {'O', '-', '-'},
                         {'O', '-', '-'}};
        assertEquals(true, OXGame.checkVertical(table, 'O', 1));
    }
    
    @Test
    public void checkVertical2() {
        char table[][] = {{'-', 'O', '-'},
                         {'-', 'O', '-'},
                         {'-', 'O', '-'}};
        assertEquals(true, OXGame.checkVertical(table, 'O', 2));
    }
    
    @Test
    public void checkVertical3() {
        char table[][] = {{'-', '-', 'O'},
                         {'-', '-', 'O'},
                         {'-', '-', 'O'}};
        assertEquals(true, OXGame.checkVertical(table, 'O', 3));
    }
    
    @Test
    public void checkHorizontal1() {
        char table[][] = {{'O', 'O', 'O'},
                         {'-', '-', '-'},
                         {'-', '-', '-'}};
        assertEquals(true, OXGame.checkHorizontal(table, 'O', 1));
    }
    
    @Test
    public void checkHorizontal2() {
        char table[][] = {{'-', '-', '-'},
                         {'O', 'O', 'O'},
                         {'-', '-', '-'}};
        assertEquals(true, OXGame.checkHorizontal(table, 'O', 2));
    }
    
    @Test
    public void checkHorizontal3() {
        char table[][] = {{'-', '-', '-'},
                         {'-', '-', '-'},
                         {'O', 'O', 'O'}};
        assertEquals(true, OXGame.checkHorizontal(table, 'O', 3));
    }
    
    @Test
    public void checkDiagonal1() {
        char table[][] = {{'O', '-', '-'},
                         {'-', 'O', '-'},
                         {'-', '-', 'O'}};
        assertEquals(false, OXGame.checkDiagonal(table, 'O'));
    }
    
    @Test
     public void checkDiagonal2() {
        char table[][] = {{'-', '-', 'O'},
                         {'-', 'O', '-'},
                         {'O', '-', '-'}};
        assertEquals(false, OXGame.checkDiagonal(table, 'O'));
    }
    
    @Test
     public void checkDraw() {
        assertEquals(true, OXGame.checkDraw(8));
    }
}
